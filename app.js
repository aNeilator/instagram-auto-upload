var fs = require('fs');
var path = require('path');




async function run(userFile) {
  var t0 = new Date().getTime();
  const {
    Chromeless
  } = require('chromeless')

  const chromeless = new Chromeless()
    var config = require('./'+userFile)

    var ordered = fs.readdirSync(config.dir);
    var images = [];
    ordered.forEach(function(file, index) {
        if ((/\.(gif|jpg|jpeg|tiff|png)$/i).test(file) &&
            /\d/.test(file)) {
            images.push(file);
        }
    });
    images.sort(function(a, b) {
        return (Number(a.match(/(\d+)/g)[0]) - Number((b.match(/(\d+)/g)[0])));
    });

    console.log("Running Instagram auto upload tool.");
    console.log("---------------------------");
    console.log("Username      : " + config.login.username);
    console.log("Image folder  : " + config.dir);
    console.log("Description   : " + config.description);
    console.log("Total images  : " + images.length);
    console.log("---------------------------");


    var sel = config.selectors;
    console.log("Logging in to Instagram");
    const login = await chromeless
        .setUserAgent('Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B137 Safari/601.1')
        .goto('https://www.instagram.com/accounts/login/')
        .setViewport({
            width: 375,
            height: 667,
            scale: 1
        })
        //enter credentials
        .type(config.login.username, sel.username)
        .type(config.login.password, sel.password)
        .click(sel.login)
        // .wait(1000)
        ;
    //not now for download app
    var exists = chromeless.exists(sel.notnow);
    console.log("Not Now exists"+exists);
    if (exists === true) {
        await chromeless
            .click(sel.notnow)
            // .wait(sel.camera)
            // .screenshot()
    }
    await chromeless.screenshot();
    // copyImg(login, "login");


    var t1 = new Date().getTime();
    console.log("Logged in [Time taken: " + ((t1 - t0) / 1000) + "s]");
    console.log("Uploading...");
    var tx, ty; // = new Date().getTime();
    for (var n = images.length - 1; n >= 0; n--) {
        tx = new Date().getTime();
        var uploadFileName = path.resolve(config.dir, images[n].toString());

        const upload = await chromeless
            //camera
            .click(sel.camera)
            // .wait(100)
            .setFileInput(sel.inputfile, uploadFileName)
            //next button
            // .wait(100)
            // .wait(sel.next)
            .click(sel.next)
            //upload
            // .wait(100)
            // .wait(sel.description)
            .type(config.description, sel.description)
            // share button
            .click(sel.share)
            // .wait(sel.camera)
            .screenshot()
            // copyImg(upload, "upload-" + n);
        var ty = new Date().getTime();
        console.log("Uploaded " + images[n].toString() +
            "    [Time taken: " + ((ty - tx) / 1000) + "s]");
    }

    console.log("Ending Chrome Server...");


    var time = new Date().getTime() - t0;
    console.log("---------------------------");
    console.log('Execution time: ' + (time / 1000) + "s");
    console.log("---------------------------");
    await chromeless.end();
    // await chromeless.clearCookies();
}

function copyImg(pathimg, name) {
    var filename = 'out-' + name + '-' + Date.now() + '.png';
    fs.rename(pathimg, path.resolve(__dirname, 'screenshots/' + filename));
    console.log("Saved file: " + filename);
}


var userFile = "config.json";
process.argv.forEach(function (val, index, array) {
  // console.log(index + ': ' + val);
  if (index == 2)
  userFile = val;
});


run(userFile); //.catch(console.error.bind(console))
// run('config_guxoj.json'); //.catch(console.error.bind(console))
 // chromeless.end()
