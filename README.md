# README #

Tool to automatically login and upload images to a given instagram account.

## How do I get set up? ##

* Make sure you have _node.js_ installed
* Change the contents of [config.json](config.json) (see [Providing Images](##providing-images))
* run `npm install`
* run `node app.js`

### Providing Images ###

* Within the `config.json`'s `dir` key, you must provide the relative path of the folder containing images that you want to upload
* The images *must* be as named numbers starting from 0,1,2...n.[jpg/png]
	* The script will upload the highest number filename first to instagram to get Instagram's 3 mosaic effect.

### Who do I talk to? ###

* Snehil Bhushan (ssnehil@hotmail.com)